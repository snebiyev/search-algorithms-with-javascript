"use strict";

function init() {
  // Just for encapsulation from other files
  // Dummy data
  console.time("linear");
  const arr1 = [1, 6, 2, 8, 3];
  const arr2 = [12, -1, 0, 3, 3];
  const arr3 = [-8, 6, 0, -1, 3, -10];
  const arr4 = [5];
  const arr5 = [];
  const arr6 = [6, 1, 2, 2, 2, 1, 5, -14];
  const arr7 = [10, 55, -5, 34, 7, 22, 19];

  const linearSearch = (arr, item) => {
    for (let i = 0; i < arr.length; i++) {
      if (item === arr[i]) {
        return i;
      }
    }
    return -1;
  };

  console.log(linearSearch(arr1, 3));
  console.log(linearSearch(arr2, 1));
  console.log(linearSearch(arr3, -10));
  console.log(linearSearch(arr4, 5));
  console.log(linearSearch(arr5, 0));

  console.timeEnd("linear");
}

init();
